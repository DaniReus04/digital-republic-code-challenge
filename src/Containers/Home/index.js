import { useState } from "react";
import Alert from "../../Components/Alert";
import AreaAlert from "../../Components/AreaAlert";
import HeighAlert from "../../Components/HeighAlert";
import Result from "../../Components/Result";

function Home({ window, door, liter }) {
  const [alert, setAlert] = useState(false);
  const [heighAlert, setHeighAlert] = useState(false);
  const [areaAlert, setAreaAlert] = useState(false);
  const [complete, setComplete] = useState(false);

  const [width1, setWidth1] = useState(1);
  const [heigh1, setHeigh1] = useState(1);

  const [w, setW] = useState(0);
  const [d, setD] = useState(0);

  const window1 = w * window;
  const door1 = d * door;
  let area1 = width1 * heigh1;
  const halfArea1 = area1 / 2;

  if (window1 > 0 && door1 === 0) {
    area1 = width1 * heigh1 - window1;
  } else if (door1 > 0 && window1 === 0) {
    area1 = width1 * heigh1 - door1;
  } else if (window1 > 0 && door1 > 0) {
    area1 = width1 * heigh1 - window1 - door1;
  }

  const dwCondition1 = Number(window1) + Number(door1);
  const hxwCondition1 = width1 * heigh1;
  if (dwCondition1 > halfArea1) {
    setAlert(true);
    setW(0);
    setD(0);
    setTimeout(() => {
      setAlert(false);
    }, 2000);
  }
  if (d > 0 && heigh1 < 2.2) {
    setHeighAlert(true);
    setHeigh1(2.2);
    setWidth1(2.2);
    setD(1);
    setTimeout(() => {
      setHeighAlert(false);
    }, 2000);
  }
  if (hxwCondition1 > 15) {
    setAreaAlert(true);
    setHeigh1(1);
    setWidth1(1);
    setTimeout(() => {
      setAreaAlert(false);
    }, 2000);
  }

  const [width2, setWidth2] = useState(1);
  const [heigh2, setHeigh2] = useState(1);

  const [w2, setW2] = useState(0);
  const [d2, setD2] = useState(0);

  const window2 = w2 * window;
  const door2 = d2 * door;
  let area2 = width2 * heigh2;
  const halfArea2 = area2 / 2;

  if (window2 > 0 && door2 === 0) {
    area2 = width2 * heigh2 - window2;
  } else if (door2 > 0 && window2 === 0) {
    area2 = width2 * heigh2 - door2;
  } else if (window2 > 0 && door2 > 0) {
    area2 = width2 * heigh2 - window2 - door2;
  }

  const dwCondition2 = Number(window2) + Number(door2);
  const hxwCondition2 = width2 * heigh2;
  if (dwCondition2 > halfArea2) {
    setAlert(true);
    setD2(0);
    setW2(0);
    setTimeout(() => {
      setAlert(false);
    }, 2000);
  }
  if (d2 > 0 && heigh2 < 2.2) {
    setHeighAlert(true);
    setHeigh2(2.2);
    setWidth2(2.2);
    setD2(1);
    setTimeout(() => {
      setHeighAlert(false);
    }, 2000);
  }
  if (hxwCondition2 > 15) {
    setAreaAlert(true);
    setHeigh2(1);
    setWidth2(1);
    setD2(1);
    setTimeout(() => {
      setAreaAlert(false);
    }, 2000);
  }

  const [width3, setWidth3] = useState(1);
  const [heigh3, setHeigh3] = useState(1);

  const [w3, setW3] = useState(0);
  const [d3, setD3] = useState(0);

  const window3 = w3 * window;
  const door3 = d3 * door;
  let area3 = width3 * heigh3;
  const halfArea3 = area3 / 2;

  if (window3 > 0 && door3 === 0) {
    area3 = width3 * heigh3 - window3;
  } else if (door3 > 0 && window3 === 0) {
    area3 = width3 * heigh3 - door3;
  } else if (window3 > 0 && door3 > 0) {
    area3 = width3 * heigh3 - window3 - door3;
  }

  const dwCondition3 = Number(window3) + Number(door3);
  const hxwCondition3 = width3 * heigh3;
  if (dwCondition3 > halfArea3) {
    setAlert(true);
    setW3(0);
    setD3(0);
    setTimeout(() => {
      setAlert(false);
    }, 2000);
  }
  if (d3 > 0 && heigh3 < 2.2) {
    setHeighAlert(true);
    setHeigh3(2.2);
    setWidth3(2.2);
    setD3(1);
    setTimeout(() => {
      setHeighAlert(false);
    }, 2000);
  }
  if (hxwCondition3 > 15) {
    setAreaAlert(true);
    setHeigh3(1);
    setWidth3(1);
    setTimeout(() => {
      setAreaAlert(false);
    }, 2000);
  }

  const [width4, setWidth4] = useState(1);
  const [heigh4, setHeigh4] = useState(1);

  const [w4, setW4] = useState(0);
  const [d4, setD4] = useState(0);

  const window4 = w4 * window;
  const door4 = d4 * door;
  let area4 = width4 * heigh4;
  const halfArea4 = area4 / 2;

  if (window4 > 0 && door4 === 0) {
    area4 = width4 * heigh4 - window4;
  } else if (door4 > 0 && window4 === 0) {
    area4 = width4 * heigh4 - door4;
  } else if (window4 > 0 && door4 > 0) {
    area4 = width4 * heigh4 - window4 - door4;
  }

  const dwCondition4 = Number(window4) + Number(door4);
  const hxwCondition4 = width4 * heigh4;
  if (dwCondition4 > halfArea4) {
    setAlert(true);
    setD4(0);
    setW4(0);
    setTimeout(() => {
      setAlert(false);
    }, 2000);
  }
  if (d4 > 0 && heigh4 < 2.2) {
    setHeighAlert(true);
    setHeigh4(2.2);
    setWidth4(2.2);
    setD4(1);
    setTimeout(() => {
      setHeighAlert(false);
    }, 2000);
  }
  if (hxwCondition4 > 15) {
    setAreaAlert(true);
    setHeigh4(1);
    setWidth4(1);
    setTimeout(() => {
      setAreaAlert(false);
    }, 2000);
  }

  const totalArea =
    hxwCondition1 + hxwCondition2 + hxwCondition3 + hxwCondition4;

  const bigger = totalArea > 3;

  const onClick = () => {
    setComplete(true);
  };

  return (
    <div className="min-h-screen py-10 bg-orange-400 font-extralight text-base">
      <div className="grid items-start justify-center ">
        <div className="bg-neutral-100 w-full h-max py-4 rounded-xl text-neutral-900">
          {complete ? (
            <Result total={totalArea / liter} />
          ) : (
            <>
              <div className="px-4 divide-y-2 divide-slate-400">
                <h1 className="font-bold">
                  Quantas latas de tinta eu preciso?
                </h1>
                <ul>
                  <li className="font-semibold">Lembre-se que:</li>
                  <li>
                    <b>Portas</b> e <b>Janelas</b> possuem os valores de{" "}
                    <b>1,90x0,80(1.52m²)</b> e <b>2,00x1,20m(2,4m²)</b>,
                    respectivamente!
                  </li>
                  <li>
                    Quando <b>houver</b> uma porta, a altura da parede deve ser{" "}
                    <b>maior que 2.2m!</b>
                  </li>
                  <li>
                    A área da parede não pode ser <b>maior que 15m²</b> ou{" "}
                    <b>menor que 1m²!</b>
                  </li>
                  <li>
                    As nossas latas de tinta são de{" "}
                    <b>18L, 3.6L, 2.5L, 0.5L!</b>
                  </li>
                </ul>
              </div>
              {areaAlert ? (
                <AreaAlert />
              ) : (
                <>
                  {heighAlert ? (
                    <HeighAlert />
                  ) : (
                    <>
                      {alert ? (
                        <Alert />
                      ) : (
                        <>
                          <p className="px-4 font-semibold py-3">
                            Com base no que foi dito acima, preencha os valores:
                          </p>
                          <div className="px-4 divide-y-2">
                            <div className="py-3">
                              <p className="font-semibold">
                                Área disponível na primeira parede: {area1}
                              </p>
                              <div>
                                <div className="flex">
                                  Largura: {width1}
                                  <p className="px-3">
                                    <input
                                      className="bg-neutral-100 px-2 font-bold"
                                      type="number"
                                      min="1"
                                      max="10"
                                      step="0.1"
                                      value={width1}
                                      onChange={(e) =>
                                        setWidth1(e.target.value)
                                      }
                                    />
                                  </p>
                                </div>
                                <div className="flex">
                                  Altura: {heigh1}
                                  <p className="px-6">
                                    <input
                                      className="bg-neutral-100 px-2 font-bold"
                                      type="number"
                                      min="1"
                                      max="10"
                                      step="0.1"
                                      value={heigh1}
                                      onChange={(e) =>
                                        setHeigh1(e.target.value)
                                      }
                                    />
                                  </p>
                                </div>
                              </div>
                              <div>
                                <div className="flex">
                                  Janelas:{" "}
                                  <p className="px-6">
                                    <input
                                      className="bg-neutral-100 px-3 font-bold"
                                      type="number"
                                      min="1"
                                      max="10"
                                      value={w}
                                      onChange={(e) => setW(e.target.value)}
                                    />
                                  </p>
                                </div>
                                <div className="flex">
                                  Portas:{" "}
                                  <p className="px-8">
                                    <input
                                      className="bg-neutral-100 px-2 font-bold"
                                      type="number"
                                      min="1"
                                      max="10"
                                      value={d}
                                      onChange={(e) => setD(e.target.value)}
                                    />
                                  </p>
                                </div>
                              </div>
                            </div>
                            <div className="py-3">
                              <p className="font-semibold">
                                Área disponível na segunda parede: {area2}
                              </p>
                              <div>
                                <div className="flex">
                                  Largura: {width2} <br />
                                  <p className="px-3">
                                    <input
                                      className="bg-neutral-100 px-2 font-bold"
                                      type="number"
                                      min="1"
                                      max="10"
                                      step="0.1"
                                      value={width2}
                                      onChange={(e) =>
                                        setWidth2(e.target.value)
                                      }
                                    />
                                  </p>
                                </div>
                                <div className="flex">
                                  Altura: {heigh2} <br />
                                  <p className="px-6">
                                    <input
                                      className="bg-neutral-100 px-2 font-bold"
                                      type="number"
                                      min="1"
                                      max="10"
                                      step="0.1"
                                      value={heigh2}
                                      onChange={(e) =>
                                        setHeigh2(e.target.value)
                                      }
                                    />
                                  </p>
                                </div>
                              </div>
                              <div>
                                <div className="flex">
                                  Janelas:{" "}
                                  <p className="px-6">
                                    <input
                                      className="bg-neutral-100 px-3 font-bold"
                                      type="number"
                                      min="1"
                                      max="10"
                                      value={w2}
                                      onChange={(e) => setW2(e.target.value)}
                                    />
                                  </p>
                                </div>
                                <div className="flex">
                                  Portas:{" "}
                                  <p className="px-7">
                                    <input
                                      className="bg-neutral-100 px-3 font-bold"
                                      type="number"
                                      min="1"
                                      max="10"
                                      value={d2}
                                      onChange={(e) => setD2(e.target.value)}
                                    />
                                  </p>
                                </div>
                              </div>
                            </div>
                            <div className="py-3">
                              <p className="font-semibold">
                                Área disponível na terceira parede: {area3}
                              </p>
                              <div>
                                <div className="flex">
                                  Largura: {width3} <br />
                                  <p className="px-3">
                                    <input
                                      className="bg-neutral-100 px-2 font-bold"
                                      type="number"
                                      min="1"
                                      max="10"
                                      step="0.1"
                                      value={width3}
                                      onChange={(e) =>
                                        setWidth3(e.target.value)
                                      }
                                    />
                                  </p>
                                </div>
                                <div className="flex">
                                  Altura: {heigh3} <br />
                                  <p className="px-6">
                                    <input
                                      className="bg-neutral-100 px-2 font-bold"
                                      type="number"
                                      min="1"
                                      max="10"
                                      step="0.1"
                                      value={heigh3}
                                      onChange={(e) =>
                                        setHeigh3(e.target.value)
                                      }
                                    />
                                  </p>
                                </div>
                              </div>
                              <div className="flex">
                                Janelas:{" "}
                                <p className="px-5">
                                  <input
                                    className="bg-neutral-100 px-4 font-bold"
                                    type="number"
                                    min="1"
                                    max="10"
                                    value={w3}
                                    onChange={(e) => setW3(e.target.value)}
                                  />
                                </p>
                              </div>
                              <div className="flex">
                                Portas:{" "}
                                <p className="px-4">
                                  <input
                                    className="bg-neutral-100 px-6 font-bold"
                                    type="number"
                                    min="1"
                                    max="10"
                                    value={d3}
                                    onChange={(e) => setD3(e.target.value)}
                                  />
                                </p>
                              </div>
                            </div>
                            <div className="py-3">
                              <p className="font-semibold">
                                Área disponível na quarta parede: {area4}
                              </p>
                              <div>
                                <div className="flex">
                                  Largura: {width4} <br />
                                  <p className="px-3">
                                    <input
                                      className="bg-neutral-100 px-2 font-bold"
                                      type="number"
                                      min="1"
                                      max="10"
                                      step="0.1"
                                      value={width4}
                                      onChange={(e) =>
                                        setWidth4(e.target.value)
                                      }
                                    />
                                  </p>
                                </div>
                                <div className="flex">
                                  Altura: {heigh4} <br />
                                  <p className="px-3">
                                    <input
                                      className="bg-neutral-100 px-5 font-bold"
                                      type="number"
                                      min="1"
                                      max="10"
                                      step="0.1"
                                      value={heigh4}
                                      onChange={(e) =>
                                        setHeigh4(e.target.value)
                                      }
                                    />
                                  </p>
                                </div>
                              </div>
                              <div className="flex">
                                Janelas:{" "}
                                <p className="px-5">
                                  <input
                                    className="bg-neutral-100 px-4 font-bold"
                                    type="number"
                                    min="1"
                                    max="10"
                                    value={w4}
                                    onChange={(e) => setW4(e.target.value)}
                                  />
                                </p>
                              </div>
                              <div className="flex">
                                Portas:{" "}
                                <p className="px-4">
                                  <input
                                    className="bg-neutral-100 px-6 font-bold"
                                    type="number"
                                    min="1"
                                    max="10"
                                    value={d4}
                                    onChange={(e) => setD4(e.target.value)}
                                  />
                                </p>
                              </div>
                            </div>
                            <div>
                              <p className="font-semibold">
                                Total de janelas:{" "}
                                {Number(w) +
                                  Number(w2) +
                                  Number(w3) +
                                  Number(w4)}
                              </p>
                              <p className="font-semibold">
                                Total de portas:{" "}
                                {Number(d) +
                                  Number(d2) +
                                  Number(d3) +
                                  Number(d4)}
                              </p>
                            </div>
                          </div>
                        </>
                      )}
                    </>
                  )}
                </>
              )}

              {bigger ? (
                <p className="flex justify-center">
                  <button
                    onClick={onClick}
                    className="flex justify-center w-56 px-2 py-1 rounded-md bg-zinc-200 font-bold pointer-events-auto"
                  >
                    Calcular
                  </button>
                </p>
              ) : (
                <p className="flex justify-center">
                  <button
                    disabled
                    className="flex justify-center w-56 px-2 py-1 rounded-md bg-zinc-200 font-bold pointer-events-auto"
                  >
                    Calcular
                  </button>
                </p>
              )}
            </>
          )}
        </div>
      </div>
    </div>
  );
}

export default Home;
