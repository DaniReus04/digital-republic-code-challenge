import React from "react";
import { Link } from "react-router-dom";

function Result() {
  return (
    <div className="min-h-screen py-56 bg-orange-400 font-extralight text-base">
      <div className="grid items-start justify-center">
        <div className="bg-neutral-100 w-80 h-48 px-8 py-4 rounded-xl text-neutral-900">
          <h1 className="font-bold">Seja Bem-Vindo!</h1>
          <p className="font-semibold py-2">
            Precisando saber quantos litros de tinta precisa para pintar uma
            sala? Está no lugar certo!
          </p>
          <div className="flex justify-center py-3">
            <Link
              to="/home"
              className="flex justify-center w-56 px-2 py-1 rounded-md bg-zinc-200 font-bold pointer-events-auto"
            >
              Vamos calcular?
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Result;
