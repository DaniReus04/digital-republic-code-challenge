import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Home from "../Home";
import Wellcome from "../Wellcome";

function App() {
  const window = 2.4;
  const door = 1.52;
  const liter = 5;

  return (
    <Router>
      <div>
        <Routes>
          <Route
            path="/home"
            element={<Home window={window} door={door} liter={liter} />}
          />
          <Route exact path="/" element={<Wellcome />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
