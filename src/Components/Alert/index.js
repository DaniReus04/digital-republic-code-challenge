const Alert = () => {
  return (
    <div
      style={{
        position: "fixed",
        left: 0,
        top: 0,
        zIndex: 1,
        height: "100%",
        width: "100%",
        backgroundColor: "white",
        opacity: 0.86,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        alignContent: "center",
        fontSize: 25,
        fontStyle: "italic",
        color: "black",
      }}
    >
      O espaço reservado para janelas e portas deve ser menor que 50% da área!
      Portas e Janelas zeradas!
    </div>
  );
};

export default Alert;
