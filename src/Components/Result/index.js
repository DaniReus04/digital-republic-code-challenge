import React from "react";
import Redirect from "../Redirect";

function Result({ total }) {
  const l2 = "3,6L";
  const l3 = "2,5L";
  const l4 = "0,5L";
  const t = parseInt(total);
  console.log("t:", t);

  if (t === 12) {
    return (
      <>
        <div className="px-4 font-semibold py-4">
          Você precisa de 3 latas de {l2} e 3 de {l4}. Seu total foi 12m²
          arredondados!
        </div>
        <Redirect />
      </>
    );
  } else if (t === 11) {
    return (
      <>
        <div className="px-4 font-semibold py-4">
          Você precisa de 3 latas de {l2} e uma de {l4}. Seu total foi de 11m²
          arredondados!
        </div>
        <Redirect />
      </>
    );
  } else if (t === 10) {
    return (
      <>
        <div className="px-4 font-semibold py-4">
          Você precisa de 2 latas de {l2}, uma de {l3} e, uma de {l4}. Seu total
          foi de 10m² arredondados!
        </div>
        <Redirect />
      </>
    );
  } else if (t === 9) {
    return (
      <>
        <div className="px-4 font-semibold py-4">
          Você precisa de 2 latas de {l2} e 4 de {l4}. Seu total foi de 9m²
          arredondados!
        </div>
        <Redirect />
      </>
    );
  } else if (t === 8) {
    return (
      <>
        <div className="px-4 font-semibold py-4">
          Você precisa de 2 latas de {l2} e 2 de {l4}. Seu total foi de 8m²
          arredondados!
        </div>
        <Redirect />
      </>
    );
  } else if (t === 7) {
    return (
      <>
        <div className="px-4 font-semibold py-4">
          Você precisa de uma lata de {l2}, uma de {l3} e 2 de {l4}. Seu total
          foi de 7m² arredondados!
        </div>
        <Redirect />
      </>
    );
  } else if (t === 6) {
    return (
      <>
        <div className="px-4 font-semibold py-4">
          Você precisa de uma lata de {l2} e uma de {l3}. Seu total foi de 6m²
          arredondados!
        </div>
        <Redirect />
      </>
    );
  } else if (t === 5) {
    return (
      <>
        <div className="px-4 font-semibold py-4">
          Você precisa de uma lata de {l2} e 3 de {l4}. Seu total foi de 5m²
          arredondados!
        </div>
        <Redirect />
      </>
    );
  } else if (t === 4) {
    return (
      <>
        <div className="px-4 font-semibold py-4">
          Você precisa de uma lata de {l2} e uma de {l4}. Seu total foi de 4m²
          arredondados!
        </div>
        <Redirect />
      </>
    );
  } else if (t === 3) {
    return (
      <>
        <div className="px-4 font-semibold py-4">
          Você precisa de uma lata de {l3} e uma de {l4}. Seu total foi 3m²
          arredondados!
        </div>
        <Redirect />
      </>
    );
  } else if (t === 2) {
    return (
      <>
        <div className="px-4 font-semibold py-4">
          Você precisa de latas de {l4}. Seu total foi de 2m² arredondados!
        </div>
        <Redirect />
      </>
    );
  } else if (t === 1) {
    return (
      <>
        <div className="px-4 font-semibold py-4">
          Você precisa de 2 latas de {l4}. Seu total foi de 1m² arredondados!
        </div>
        <Redirect />
      </>
    );
  }
}

export default Result;
