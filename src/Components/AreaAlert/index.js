const AreaAlert = () => {
  return (
    <div
      style={{
        position: "fixed",
        left: 0,
        top: 0,
        zIndex: 1,
        height: "100%",
        width: "100%",
        backgroundColor: "white",
        opacity: 0.86,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        alignContent: "center",
        fontSize: 25,
        fontStyle: "italic",
        color: "black",
      }}
    >
      A área não pode ser superior a 15m²! Valores resetados!
    </div>
  );
};

export default AreaAlert;
