import React from "react";
import { Link } from "react-router-dom";

function Message() {
  return (
    <div className="flex justify-center">
      <Link
        to="/"
        className="flex justify-center w-56 px-2 py-1 rounded-md bg-zinc-200 font-bold pointer-events-auto"
      >
        Calcular denovo
      </Link>
    </div>
  );
}

export default Message;
